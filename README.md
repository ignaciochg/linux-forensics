# ProRay-LF

# Install dependecies

* docker
* docker-compose


# Mount FS

Mount linux fs into `mnt-here` folder before executing software

## Mounting
So far this step has to be done manually. 
here is an example to mount VirtualBox VDI images.

```bash
# install dependencies
sudp apt-get install qemu
# more info 
# https://askubuntu.com/a/50290/1156338

cd mount-tools
sh ./mount-vdi.sh <location-to-vdi>

# find out what partition you want by mounting into
# system using regular file explorer
# use fdisk to find what partiton you want
# look into partitons for /dev/nbd0
sudo fdisk -l # we want /dev/nbd0p5 in my case

# mount it
sudo mount /dev/nbd0p5 <full-poject-path>/mnt-here

# after we are done using it
sudo umount /dev/nbd0p5
sh ./umount-vdi.sh
```

# Running

```bash
cd <project folder>
docker-compose up --build
```   

This will bring up the software and the database. use `ctrl-c` to exit. Exiting will also stop the database.   
THe first time running the software  it will build the environment, this can take some time. Afther running software once the `--build` flag is no longer needed as proray-lf is already built in the system. 

## Fail to run due to DB permission error
If the software refuses to run because of database permissions, run with admin permissions:
```bash
cd <project folder>
sudo docker-compose up --build
```

# Output

output is the `ouput` folder. This is your case results, share this around. If farther analysys is also required also share image.   
The output folder contains the `db` which is not human usable(see view database) and any other files extracted by software.

## How to actually view database
Use any mongodb client to connect and view it.    
Recomended `Robo 3T`.   
When the databse is up, it exposes to port `5005` on the `localhost` interface.    
The connection URI is `mongodb://localhost:5005`   

# Bring DB up

If it is desired to view database after running the software and the initial databse has been stoped then use the following command to vring it back up:   
```bash
cd <project-root>
docker-compose up db
```

# Importing case

* Copy contents of case `output` folder into `output` in project root
* Mount original image to `mnt-here` folder
*  Rerun db/software as desired.


