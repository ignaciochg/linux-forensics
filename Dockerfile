FROM ubuntu:18.04

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y vim

RUN apt install --yes python3 python3-dev python3-setuptools python3-pip python3-venv

RUN apt-get install -y qemu kpartx && \
    apt-get install -y foremost && \
    apt-get install -y exfat-fuse exfat-utils

# prevent this from being interactive
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install tzdata

# install scripts dependencies
RUN apt-get install -y binwalk

# setup environment & install dependencies
COPY ./requirements.txt /linux-forensics/requirements.txt
RUN pip3 install -r /linux-forensics/requirements.txt

# misc
RUN mkdir -p /input
RUN mkdir -p /output
RUN mkdir -p /workdir


# copy linux-forensics code last
COPY ./linux-forensics /linux-forensics

WORKDIR /linux-forensics

CMD ["/usr/bin/python3", "/linux-forensics/main.py"] 

