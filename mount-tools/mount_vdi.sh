#!/usr/bin/env bash

sudo modprobe nbd max_part=16
echo $1
sudo qemu-nbd -c /dev/nbd0 "$1"
#sudo qemu-nbd -d /dev/nbd0
