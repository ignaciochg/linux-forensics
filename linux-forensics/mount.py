#!/usr/bin/env python3  

import sys
import os
import subprocess

# https://linux.m2osw.com/mounting-vdi-disk-your-host-system-edit-file-system
# https://askubuntu.com/questions/19430/mount-a-virtualbox-drive-image-vdi

def run(command, printERR=True):
   process = subprocess.Popen(command.split(), stdout=subprocess.PIPE,stderr=subprocess.PIPE)
   output, error = process.communicate()
   if printERR and error:
      print("ERROR",error)
   return output, error

def run_list(command_list, printERR=True):
   process = subprocess.Popen(command, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
   output, error = process.communicate()
   if printERR and error:
      print("ERROR",error)
   return output, error


def losetup(file):
   output, error = run("losetup --find --show "+file)

   if error:
      sys.exit("losetup({}) output error: {}".format(file,error.decode()))
      
   else:
      ret = output.decode().replace("\n",'')
      print("losetup({}) output: {}".format(file,ret))
      return ret


def basename(path):
   output, error = run("basename "+path)

   if error:
      print("basename({}) output error: {}".format(path,error.decode()))
   else:
      ret = output.decode().replace("\n",'')
      print("basename({}) output: {}".format(path,ret))
      return ret 

def get_fs_type(dev_path):
   output, err = run("file -sL {}".format(dev_path))

   out = output.decode().lower()
   if "ext4" in out:
      return "ext4"
   elif "ext3" in out:
      return "ext3"
   elif "ext2" in out:
      return "ext2"
   return None
   

def mount_dev(fs_type, dev_path, mountpoint):
   output, error = run("mount -t {} {} {}".format(fs_type, dev_path, mountpoint))

   if error:
      sys.exit("mount_dev({},{},{}) output error: {}".format(fs_type,dev_path,mountpoint,error.decode()))
      
   else:
      ret = output.decode().replace("\n",'')
      print("mount_dev({},{},{}) output: {}".format(fs_type,dev_path,mountpoint,ret))
      return ret

def get_blkid():
   output, error = run("blkid")

   if error:
      sys.exit("blkid output error: {}".format(error.decode()))
      
   else:
      ret = output.decode().splitlines()
      # print("get_blkid output: {}".format(ret))
      return ret

def get_fdisk():
   output, error = run("fdisk -l")

   if error:
      sys.exit("fdisk output error: {}".format(error.decode()))
      
   else:
      ret = output.decode().splitlines()
      # print("get_blkid output: {}".format(ret))
      return ret

def partprobe(dev_path):
   output, error = run("partprobe {}".format(dev_path))

   if error:
      print("partprobe({}) output error: {}".format(dev_path,error.decode()))
      return False
   else:
      ret = output.decode()
      print("partprobe({}) output: {}".format(dev_path,ret))
      return True


def get_dev_partitions(dev_path):
   # partprobe(dev_path)
   blkid_output = get_blkid()
   # print(blkid_output)
   all_devs = [blk_entry.split(":")[0] for blk_entry in blkid_output]
   parts = list()
   for dev in all_devs:
      if dev_path in dev:
         parts.append(dev)
   return parts

def get_dev_partitions_fdisk(dev_path):
   # partprobe(dev_path)
   blkid_output = get_fdisk()
   # print(blkid_output)
   all_devs = [blk_entry.split(" ")[0] for blk_entry in blkid_output]
   parts = list()
   for dev in all_devs:
      if dev_path in dev:
         parts.append(dev)
   return parts

def mount_file(file_name):
   loop_dev = losetup(file_name)
   if not loop_dev:
      return None




if __name__ == '__main__':
   pass
