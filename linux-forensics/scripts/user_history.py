#!/usr/bin/env python3  

from pathlib import Path

try:
   from common import get_cwd, chdir, get_collection, smash_path
except ModuleNotFoundError:
   from .common import get_cwd, chdir, get_collection, smash_path


def get_hist_locations(path):
   locs = set(list(path.glob(".*_history")) + list(path.glob("*hist*")))
   return list(locs)

def retrive_hist_info(file):
   try:
      return open(file, "r").read().splitlines()
   except:
      return None

def remove_prefix(text, prefix):
   if text.startswith(prefix):
      return text[len(prefix):]
   return text

def get_user_history(base_path, username, home_dir):
   full_home_path = smash_path(base_path, home_dir)

   #defualt ones
   hist_locations = [smash_path(full_home_path, l) for l in ['.bash_history','.histfile']]
   more_hist_locs = get_hist_locations(full_home_path)
   
   all_locations  = list(set(hist_locations + more_hist_locs))

   histories = list()
   for loc in all_locations:
      hist_info  = retrive_hist_info(loc)
      if hist_info:
         location = remove_prefix(str(loc), str(base_path))
         if location[0] != '/':
            location = '/'+location
         dat = {"username":username,"path":location, "history": hist_info}
         histories.append(dat)
   
   if len(histories) == 0:
      return None
   return histories

def main(base_path,dbname, database_uri="mongodb://db"):
   cwd_bak=get_cwd()
   
   base_p = Path(base_path)

   print("Collecting user history information...",end="",flush=True)

   user_info = get_collection(database_uri, "All Users",dbname).find({})

   hist_db = get_collection(database_uri, "User History",dbname)

   for record in user_info:
      username = record["username"]
      if "home_directory" in record:
         home_dir = record["home_directory"]
         user_hist = get_user_history(base_path, username, home_dir)
         if user_hist:
            # print(user_hist)
            hist_db.insert_many(user_hist)
   

   print("Done",flush=True)

   chdir(cwd_bak)
   return 0



if __name__ == '__main__':
   main("/workdir", "mongodb://db")
