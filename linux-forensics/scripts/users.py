#!/usr/bin/env python3  

from pathlib import Path

try:
   from common import run, create_dir, get_cwd, chdir, get_collection, smash_path
except ModuleNotFoundError:
   from .common import run, create_dir, get_cwd, chdir, get_collection, smash_path

from pprint import pprint
import traceback

def get_hash_type(hash):
   types = {
      "$1$":"MD5",
      "$2a$":"Blowfish",
      "$2y$":"Eksblowfish",
      "$5$":"SHA-256",
      "$6$":"SHA-512"
   }

   for typ in types.keys():
      if hash.startswith(typ):
         return types[typ]
   return None

def parse_geco(info):
   if info.strip() == '':
      return None
   try:
      sp = info.split(",")
      entry = dict()

      entry["raw_geco"] = info
      try:
         entry["full_name"] = sp[0]
      except:
         pass
      try:
         entry["location"] = sp[1]
      except:
         pass
      try:
         entry["office_number"] = sp[2]
      except:
         pass
      try:
         entry["home_number"] = sp[3]
      except:
         pass
      try:
         entry["other"] = sp[4]
      except:
         pass

      # print(entry)
      return entry
   except:
      traceback.print_exc()
      return None


def get_passwd(base_path):
   location = "/etc/passwd"
   full_path = smash_path(base_path, location)
   lines = open(full_path, "r").readlines()

   records = list()

   for line in lines:
      line = line.strip()
      if line[0] == "#":
         continue
      entry = dict()

      sp = line.split(":")
      entry["record_type"] = "passwd"
      entry["raw"] = line
      entry["username"] = sp[0]
      entry["password"] = sp[1]
      entry["user_id"] = sp[2]
      entry["group_id"] = sp[3]
      entry["user_info"] = sp[4]
      entry["home_directory"] = sp[5]
      entry["shell"] = sp[6]

      
      user_info = parse_geco(sp[4])
      if user_info:
         entry["user_info"] = user_info

      records.append(entry)
   return records

def get_shadow(base_path):
   location = "/etc/shadow"
   full_path = smash_path(base_path, location)
   lines = open(full_path, "r").readlines()

   records = list()

   for line in lines:
      line = line.strip()
      if line[0] == "#":
         continue
      entry = dict()

      sp = line.split(":")
      entry["record_type"] = "shadow"
      entry["raw"] = line
      entry["username"] = sp[0]
      entry["password_hash"] = sp[1]
      entry["last_pass_change"] = sp[2]
      entry["min_password_age"] = sp[3]
      entry["max_password_age"] = sp[4]
      entry["warning_period"] = sp[5]
      entry["inactivity_period"] = sp[6]
      entry["expiration_date"] = sp[7]
      entry["unused_field"] = sp[8]

      
      hash_type = get_hash_type(sp[1])
      if hash_type:
         entry["hash_type"] = hash_type

      records.append(entry)
   return records

def get_group(base_path):
   location = "/etc/group"
   full_path = smash_path(base_path, location)
   lines = open(full_path, "r").readlines()

   records = list()

   for line in lines:
      line = line.strip()
      if line[0] == "#":
         continue
      entry = dict()

      sp = line.split(":")
      entry["record_type"] = "group"
      entry["raw"] = line
      entry["group_name"] = sp[0]
      entry["password"] = sp[1]
      entry["group_id"] = sp[2]
      entry["group_list_raw"] = sp[3]
      entry["group_list"] = sp[3].split(",")
  

      records.append(entry)
   return records


def find_user_groups(username, groups):
   gs = list()
   for g in groups:
      if username in g["group_list"]:
         gs.append({"gid":g["group_id"],"group_name":g["group_name"]})
   return gs
def combine_passwd_shadow_group(passwd, shadow, group):
   entries = list()
   for pas in passwd:
      if pas["password"].lower() == "x":
         for shad in shadow:
            if pas["username"] == shad["username"]:
               temp = {**pas, **shad}
               del temp["raw"]
               del temp["record_type"]
               break
      else:
         del pas["raw"]
         del pas["record_type"]
         temp = pas
      temp["groups"] = find_user_groups(temp["username"], group)
      entries.append(temp)

   return entries

def main(base_path, dbname, database_uri="mongodb://db"):
   cwd_bak=get_cwd()
   
   base_p = Path(base_path)

   print("Collecting user information...",flush=True)

   
   print("\tGetting /etc/passwd...",end="",flush=True)
   passwd_entried = get_passwd(base_path)
   print("Done\n\tGetting /etc/shadow...",end="",flush=True)
   shadow_entries = get_shadow(base_path)
   print("Done\n\tGetting /etc/group...",end="",flush=True)
   groups = get_group(base_path)
   print("Done",flush=True)

   users = combine_passwd_shadow_group(passwd_entried,shadow_entries, groups)

   get_collection(database_uri, "/etc/passwd",dbname).insert_many(passwd_entried)
   get_collection(database_uri, "/etc/shadow",dbname).insert_many(shadow_entries)
   get_collection(database_uri, "/etc/group",dbname).insert_many(groups)
   for u in users:
      try:
         del u["_id"]
      except:
         pass

   # for u in users:
   #    print(u["username"])
   get_collection(database_uri, "All Users",dbname).insert_many((users))

   chdir(cwd_bak)
   return 0



if __name__ == '__main__':
   main("/workdir", "mongodb://db")
