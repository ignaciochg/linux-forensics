#!/usr/bin/env python3

import os
import subprocess
from pathlib import Path
import pymongo 


def run(command, printERR=True):
   process = subprocess.Popen(command.split(), stdout=subprocess.PIPE,stderr=subprocess.PIPE)
   output, error = process.communicate()
   if printERR and error:
      print("ERROR",error)
   return output, error

def run_list(command_list, printERR=True):
   process = subprocess.Popen(command, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
   output, error = process.communicate()
   if printERR and error:
      print("ERROR",error)
   return output, error

def get_cwd():
   return os.getcwd()

def chdir(path):
   return os.chdir(path)


def install_package(package_s):
   if type(package_s) == list:
      package_s = " ".join(package_s)
   run("apt-get install -y {}".format(package_s))


def create_dir(base_path, mode=0o777, parents=True, exist_ok=True):
   p = Path(base_path)
   p.mkdir(mode=mode,parents=parents, exist_ok=exist_ok)
   return p


def md5(path):
   output,error = run("md5sum {}".format(path))
   if error:
      return None

   return output.decode().split()[0]


def get_collection(uri, col_name, db_name="main", connect=False):
   myclient = pymongo.MongoClient(uri,connect=connect)
   mydb = myclient[db_name]
   mycol = mydb[col_name]
   return mycol


def smash_path(base_path, full_path):
   p = Path(base_path)

   if full_path[0] == '/':
      full_path = full_path[1:]

   return p/full_path