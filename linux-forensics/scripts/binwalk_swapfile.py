#!/usr/bin/env python3  

from pathlib import Path

try:
   from common import run, create_dir, get_cwd, chdir, install_package, smash_path
except ModuleNotFoundError:
   from .common import run, create_dir, get_cwd, chdir, install_package, smash_path



def main(base_path, swapfile_path, output_path, flags="-q"):
   cwd_bak=get_cwd()
   
   base_p = Path(base_path)
   out_path = create_dir(output_path, parents=True, exist_ok=True)
   
   chdir(str(out_path))

   print("Binwalking swapfile")

   try:
      run("binwalk --help")
   except FileNotFoundError:
      print("Could not find binwalk, installing ...",end="")
      install_package("binwalk")
      print("Done")

   full_swapfile_path = str(smash_path(base_p, swapfile_path))
   print("Binwalk swapfile at:", full_swapfile_path)
   print("This will take a while...")
   output,error  = run("binwalk -e {} {}".format(flags, full_swapfile_path))
   
   
   chdir(cwd_bak)
   if not error:
      return 0
   return 1


if __name__ == '__main__':
   main("/workdir", "/swapfile", "/output/", flags="")
