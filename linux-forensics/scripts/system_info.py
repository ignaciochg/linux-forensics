#!/usr/bin/env python3  

from pathlib import Path

try:
   from common import run, create_dir, get_cwd, chdir, get_collection, smash_path
except ModuleNotFoundError:
   from .common import run, create_dir, get_cwd, chdir, get_collection, smash_path

def get_hostname(base_path):
   return open(Path(base_path)/"etc/hostname", "r").read()
def get_timezone(base_path):
   return open(Path(base_path)/"etc/timezone", "r").read()

def get_fstab(base_path, mongo_uri,dbname):
   location = "/etc/fstab"
   full_path = smash_path(base_path, location)
   db = get_collection(mongo_uri, "fstab",dbname)

   lines = open(full_path, "r").readlines()
   for line in lines:
      if line[0] == "#":
         continue
      file_system,mnt_pnt, typ, opts, dump, pas = line.strip().split()
      record = {
                  "file_system": file_system,
                  "mount_point": mnt_pnt,
                  "type": typ,
                  "options": opts,
                  "dump": dump,
                  "pass": pas
      }
      db.insert_one(record)


def any_startswith(l,strng):
   for element in l:
      if strng.startswith(element):
         return True
   return False

def get_grub_default(base_path, mongo_uri):
   location = "/etc/default/grub"
   full_path = smash_path(base_path, location)
   text = "GRUB_DEFAULT="
   lines = open(full_path, "r").readlines()

   for line in lines:
      if line.startswith(text):
         return line[len(text):].strip()
   return None

def get_grub(base_path, mongo_uri,dbname):
   # todo fix bug: does not parse corrently when entering subentries
   location = "/boot/grub/grub.cfg"

   kw_menuentry = ["menuentry", "submenu"]
   kw_fields = ["set root=", "linux", "initrd"]

   full_path = smash_path(base_path, location)

   lines = open(full_path, "r").readlines()

   grub_data = list()
   i = 0
   while i < len(lines):
      # print(lines[i])
      if any_startswith(kw_menuentry, lines[i].strip()):
         entry = {}
         entry["raw"] = [lines[i]]
         entry["name"] = lines[i]
         i+=1
         current_line = lines[i]
         while "}" != current_line.strip():
            entry["raw"].append(lines[i])
            # print(current_line.strip()[:5])
            if "linux" == current_line.strip()[:5]:
               f = current_line.strip().split(maxsplit=1)
               if f[0] == 'linux':
                  entry["linux"] = f[1]
            elif "initrd" == current_line.strip()[:6]:
               f = current_line.strip().split(maxsplit=1)[1]
               entry["initrd"] = f
            elif "set root=" == current_line.strip()[:9]:
                  f = current_line.strip()[9:]
                  entry["root"] = f
            try:
               i+=1
               current_line = lines[i]
            except:
               break
         entry["raw"].append("}")
         grub_data.append(entry)
         print(".",end="",flush=True)

         continue
      i+=1

   db = get_collection(mongo_uri, "grub_entries",dbname)
   for entry in grub_data:
      db.insert_one(entry)


def main(base_path,dbname, database_uri="mongodb://db"):
   cwd_bak=get_cwd()
   
   base_p = Path(base_path)

   print("Collecting system information...",flush=True)
   db = get_collection(database_uri, "system_info",dbname)

   data = {}
   
   print("\tGetting hostname...",end="",flush=True)
   data["hostname"] = get_hostname(base_p)
   print("Done\n\tGetting timezone...",end="",flush=True)
   data["timezone"] = get_timezone(base_p)
   print("Done\n\tGetting default grub entry...",end="",flush=True)
   data["grub_default"] = get_grub_default(base_path, database_uri)
   print("Done\n\tGetting grub entries...",end="",flush=True)
   get_grub(base_p, database_uri,dbname)
   print("Done\n\tGetting fstab...",end="",flush=True)
   get_fstab(base_path, database_uri,dbname)
   print("Done",flush=True)

   db.insert_one(data)

   chdir(cwd_bak)
   return 0



if __name__ == '__main__':
   main("/workdir", "mongodb://db")
