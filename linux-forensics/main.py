#!/usr/bin/env python3  

import mount
import scripts
from art import tprint
from pathlib import Path

def mount_check(base_path):
   s = len(list(base_path.iterdir()))
   if s <= 1:
      if (base_path/"remove_this_file").exists() or s == 0:
         import sys
         print("\nMount Linux filesystem into mnt-here directory... Press Ctrl+C to exit.\n",flush=True)
         sys.exit(1)

if __name__ == '__main__':
   
   # dev_path = "/dev/lf-input"

   # # parts = mount.get_dev_partitions_fdisk(dev_path)
   # # # print(parts)
  
   # # fstype_devpath = list()
   # # for dp in parts:
   # #    if len(parts) != 1 and dp == dev_path:
   # #       continue
   # #    fstype_devpath.append((mount.get_fs_type(dp),dp))

   # # print(fstype_devpath)

   # fs_type = mount.get_fs_type(dev_path)
   # mount.mount_dev(fs_type, dev_path, "/workdir")

   base_path = Path("/workdir")
   db_name = "Report"

   # tprint("Linux Forensics","rand")
   tprint("ProRay-LF","rand")
   print("Press Ctrl+C to exit.")

   mount_check(base_path)

   scripts.system_info.main(base_path,db_name)
   scripts.users.main(base_path,db_name)
   scripts.user_history.main(base_path,db_name)
   # scripts.binwalk_swapfile.main("/workdir", "/swapfile", "/output/", flags="")

   print("\nDone Running... Press Ctrl+C to exit.")